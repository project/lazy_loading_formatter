(function ($, Drupal) {
  Drupal.behaviors.lazy_loading_formatter = {
    attach: function (context, settings) {
      window.lazySizesConfig = window.lazySizesConfig || {};
      if (settings.lazy_loading_formatter.lazy_class) {
        window.lazySizesConfig.lazyClass = settings.lazy_loading_formatter.lazy_class;
      }
      if (settings.lazy_loading_formatter.lazy_src_attr) {
        window.lazySizesConfig.srcAttr = settings.lazy_loading_formatter.lazy_src_attr;
      }
      if (settings.lazy_loading_formatter.lazy_srcset_attr) {
        window.lazySizesConfig.srcSetAttr = settings.lazy_loading_formatter.lazy_srcset_attr;
      }
      window.lazySizesConfig.loadHidden = settings.lazy_loading_formatter.lazy_load_hidden;
      window.lazySizesConfig.ricTimeout = 0;
      if (settings.lazy_loading_formatter.lazy_throttle_delay) {
        window.lazySizesConfig.throttleDelay = settings.lazy_loading_formatter.lazy_throttle_delay;
      }
      window.lazySizesConfig.init = false;
      lazySizes.init();
      $(document).once('lazy_loading_formatter').on('lazyloaded', function (e) {
        if ($.fn.matchHeight) {
          $.fn.matchHeight._update();
        }
        if (settings.lazy_loading_formatter.objectfit_polyfill) {
          objectFitPolyfill();
        }
      });
    }
  };
}(jQuery, Drupal));
