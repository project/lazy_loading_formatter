## Lazy Loading Formatter

This module provides lazy loading formatters for images
and responsive images.

### Features

* Formatter for lazy loading Responsive image.
* Formatter for lazy loading image.
* Lazy loading settings can be configured globally.
* Lazy load hidden images.
* Also provides object-fit polyfill option.

### Requirements

* Requires image and responsive_image Drupal core modules.

### Install/Usage

* Install module like any other contributed module.
* Configure lazy loading settings here:
**/admin/config/lazy-loading-formatter**
* Change responsive image or image field display formatter to
one of the provided lazy loading formatters.
* Load entity or page to test out lazy loading on your images.

### Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
