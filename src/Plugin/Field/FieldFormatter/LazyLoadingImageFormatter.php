<?php

namespace Drupal\lazy_loading_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the 'lazy_loading_image_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "lazy_loading_image_formatter",
 *   label = @Translation("Lazy image"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class LazyLoadingImageFormatter extends ImageFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'object_fit' => 'cover',
        'object_fit_position' => '',
      ] + parent::defaultSettings();
  }

  /**
   * Get object-fit options.
   *
   * @return array
   */
  private function getObjectFitOptions() {
    return [
      'none' => $this->t('None'),
      'cover' => $this->t('Cover'),
      'contain' => $this->t('Contain'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('lazy_loading_formatter.settings');
    $elements = parent::settingsForm($form, $form_state);
    if ($config->get('objectfit_polyfill_enabled')) {
      $elements['object_fit'] = [
        '#title' => $this->t('Object fit'),
        '#type' => 'select',
        '#default_value' => $this->getSetting('object_fit') ?: NULL,
        '#options' => $this->getObjectFitOptions(),
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    if ($this->getSetting('object_fit')) {
      $summary[] = $this->t('Object fit: @object_fit', ['@object_fit' => $this->getObjectFitOptions()[$this->getSetting('object_fit')]]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $config = \Drupal::config('lazy_loading_formatter.settings');
    $elements = parent::viewElements($items, $langcode);
    foreach ($elements as &$element) {
      $element["#item_attributes"] = [
        'data-use-lazy-loading' => TRUE,
        'class' => $config->get('lazy_class'),
      ];
      if ($config->get('objectfit_polyfill_enabled')) {
        $element["#item_attributes"]['data-object-fit'] = $this->getSetting('object_fit') ?: TRUE;
      }
    }
    return $elements;
  }

}
