<?php

namespace Drupal\lazy_loading_formatter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LazyLoadingFormatterConfigForm
 *
 * @package Drupal\lazy_loading_formatter\Form
 */
class LazyLoadingFormatterConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lazy_loading_formatter_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['lazy_loading_formatter.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('lazy_loading_formatter.settings');

    $form['general'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('General Settings'),
    ];

    $form['general']['image_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Lazy Loading for image.'),
      '#description' => $this->t('Enable/disable lazy loading for image.'),
      '#default_value' => !empty($config->get('image_enabled')) ? $config->get('image_enabled') : 0,
    ];

    $form['general']['responsive_image_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Lazy Loading for responsive image.'),
      '#description' => $this->t('Enable/disable lazy loading for responsive image.'),
      '#default_value' => !empty($config->get('responsive_image_enabled')) ? $config->get('responsive_image_enabled') : 0,
    ];

    $form['general']['objectfit_polyfill_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable object-fit polyfill.'),
      '#description' => $this->t('Enable/disable object-fit polyfill for lazy images.'),
      '#default_value' => !empty($config->get('objectfit_polyfill_enabled')) ? $config->get('objectfit_polyfill_enabled') : 0,
    ];

    $form['config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Lazy Load Configuration'),
    ];

    $form['config']['lazy_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lazy load class'),
      '#description' => $this->t('Class used to trigger lazy loading.'),
      '#required' => TRUE,
      '#default_value' => !empty($config->get('lazy_class')) ? $config->get('lazy_class') : 'lazyload',
    ];

    $form['config']['lazy_src_attr'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lazy load source attribute'),
      '#description' => $this->t('Data attribute used for image source.'),
      '#required' => TRUE,
      '#default_value' => !empty($config->get('lazy_src_attr')) ? $config->get('lazy_src_attr') : 'data-src',
    ];

    $form['config']['lazy_srcset_attr'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lazy load source set attribute'),
      '#description' => $this->t('Data attribute used for image source set.'),
      '#required' => TRUE,
      '#default_value' => !empty($config->get('lazy_srcset_attr')) ? $config->get('lazy_srcset_attr') : 'data-srcset',
    ];

    $form['config']['lazy_load_hidden'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Lazy load hidden images'),
      '#description' => $this->t('Enable/disable lazy loading of hidden images (visibility:hidden)'),
      '#default_value' => !empty($config->get('lazy_load_hidden')) ? $config->get('lazy_load_hidden') : 1,
    ];

    $form['config']['lazy_throttle_delay'] = [
      '#type' => 'number',
      '#min' => 66,
      '#max' => 200,
      '#title' => $this->t('Lazy load throttle delay'),
      '#description' => $this->t('Throttle delay on listeners.'),
      '#default_value' => !empty($config->get('lazy_throttle_delay')) ? $config->get('lazy_throttle_delay') : 125,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('lazy_loading_formatter.settings')
      ->set('image_enabled', $form_state->getValue('image_enabled'))
      ->set('responsive_image_enabled', $form_state->getValue('responsive_image_enabled'))
      ->set('objectfit_polyfill_enabled', $form_state->getValue('objectfit_polyfill_enabled'))
      ->set('lazy_class', $form_state->getValue('lazy_class'))
      ->set('lazy_src_attr', $form_state->getValue('lazy_src_attr'))
      ->set('lazy_srcset_attr', $form_state->getValue('lazy_srcset_attr'))
      ->set('lazy_load_hidden', $form_state->getValue('lazy_load_hidden'))
      ->set('lazy_throttle_delay', $form_state->getValue('lazy_throttle_delay'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
