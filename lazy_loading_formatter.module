<?php

/**
 * @file
 * Contains lazy_loading_formatter.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function lazy_loading_formatter_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.lazy_loading_formatter':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provides Lazy loading responsive image and image field formatters.') . '</p>';
      return $output;
    default:
  }
}

/**
 * Implements hook_page_attachments_alter().
 */
function lazy_loading_formatter_page_attachments_alter(array &$attachments) {
  $config = \Drupal::config('lazy_loading_formatter.settings');
  if ($config->get('image_enabled') || $config->get('responsive_image_enabled')) {
    // Make sure it's not a admin route.
    $is_admin = \Drupal::service('router.admin_context')->isAdminRoute();
    if (!$is_admin) {
      // Attach library.
      $attachments['#attached']['library'][] = 'lazy_loading_formatter/lazy_loading_formatter.base';
      if ($config->get('objectfit_polyfill_enabled')) {
        $attachments['#attached']['library'][] = 'lazy_loading_formatter/objectfit_polyfill';
      }
      // Attach drupal settings.
      $attachments['#attached']['drupalSettings']['lazy_loading_formatter'] = [
        'lazy_class' => !empty($config->get('lazy_class')) ? $config->get('lazy_class') : 'lazyload',
        'lazy_src_attr' => !empty($config->get('lazy_src_attr')) ? $config->get('lazy_src_attr') : 'data-src',
        'lazy_srcset_attr' => !empty($config->get('lazy_srcset_attr')) ? $config->get('lazy_srcset_attr') : 'data-srcset',
        'lazy_load_hidden' => !empty($config->get('lazy_load_hidden')) ? $config->get('lazy_load_hidden') : 1,
        'lazy_throttle_delay' => !empty($config->get('lazy_throttle_delay')) ? $config->get('lazy_throttle_delay') : 125,
        'objectfit_polyfill' => !empty($config->get('objectfit_polyfill_enabled')) ? $config->get('objectfit_polyfill_enabled') : FALSE,
      ];
    }
  }
}

/**
 * Implements hook_preprocess_image_formatter().
 */
function lazy_loading_formatter_preprocess_image_formatter(&$variables) {
}

/**
 * Implements hook_preprocess_image().
 */
function lazy_loading_formatter_preprocess_image(&$variables) {
  $config = \Drupal::config('lazy_loading_formatter.settings');
  if ($config->get('image_enabled')) {
    $is_admin = \Drupal::service('router.admin_context')->isAdminRoute();
    $is_admin_theme = (\Drupal::theme()->getActiveTheme()->getName() === 'seven');
    if (!$is_admin && !$is_admin_theme) {
      if (is_array($variables["attributes"]) && array_key_exists("data-use-lazy-loading", $variables["attributes"]) && !empty($variables["attributes"]["data-use-lazy-loading"])) {
        $variables['attributes'][$config->get('lazy_src_attr')] = $variables['attributes']['src'];
        $variables['attributes']['src'] = NULL;
      }
    }
  }
}

/**
 * Implements hook_preprocess_responsive_image().
 */
function lazy_loading_formatter_preprocess_responsive_image(&$variables) {
  $config = \Drupal::config('lazy_loading_formatter.settings');
  if ($config->get('responsive_image_enabled')) {
    if (is_array($variables["attributes"]) && array_key_exists("data-use-lazy-loading", $variables["attributes"]) && !empty($variables["attributes"]["data-use-lazy-loading"])) {
      /* @var \Drupal\Core\Template\Attribute[] $variables ['sources'] */
      foreach ($variables['sources'] as $key => $source) {
        $srcset = $source->offsetGet('srcset');
        $source->offsetSet('srcset', NULL);
        $source->offsetSet($config->get('lazy_srcset_attr'), $srcset);
        $variables['sources'][$key] = $source;
      }
    }
  }
}
